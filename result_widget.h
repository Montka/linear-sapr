#ifndef RESULT_WIDGET_H
#define RESULT_WIDGET_H

#include <QWidget>

namespace Ui {
class Result_widget;
}

class Result_widget : public QWidget
{
    Q_OBJECT

public:
    explicit Result_widget(QWidget *parent = 0);
    ~Result_widget();

private:
    Ui::Result_widget *ui;
};

#endif // RESULT_WIDGET_H
