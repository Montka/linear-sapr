#ifndef PROCESSOR_MODEL_H
#define PROCESSOR_MODEL_H
#include<string>
#include"system_model.h"
class processor_model
{
    double** tableA;
    double*  tableB;
    int count;
    System_model* System_pointer;
public:
    void Copy_table(double**, double*, int);

    processor_model(System_model*);
    ~processor_model();

    void Jordan_Gauss();
    double delta(int p);
    double u(int p, double x);
    double N(int p, double x);
    double sigma(int p, double x);
    private:
  /*double a(int);
    double b(int);
    double y(int);
    double d(int);
    double Step(double, double, int);
 */
};

class processor_expt
{
public:
    std::string msg;
    explicit processor_expt(std::string str):msg(str)
    {}
};
#endif // PROCESSOR_MODEL_H
