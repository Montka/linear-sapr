//#include <QWidget>
//#include <QGraphicsScene>
//#include <QColor>
#include <iostream>
#include <QFileDialog>
#include "widget.h"
#include "ui_widget.h"
#include "tabwidget.h"
#include <math.h>


void Widget::DrawArrow(QColor color,int x1, int x2)
{
    QPen pen(color);
    scene->addLine(x1,155,x2,155,pen);
    scene->addLine(x2,155,x2+(x1-x2)/5,155-abs(x1-x2)/5,pen);
    scene->addLine(x2,155,x2+(x1-x2)/5,155+abs(x1-x2)/5,pen);
 }

void Widget::DrawLinLoad(int x1, int x2)
{
    int length=(x2-x1)/5;
    int pos=x1;
    for(int i=0; i<5; ++i)
    {
        DrawArrow(Qt::blue,pos,pos+length);
        pos+=length;
    }

}

void Widget::DrawEdge(bool right, int x, int h)
{
    scene->addLine(x,155-h,x,155+h);
    //scene->addLine()
    int offset=right?10:-10;
    for(int i=155-h;i<155+h;i+=10)
    {
        scene->addLine(x,i,x+offset,i-10);
    }
}
void Widget::Draw()
{
    int length=System_pointer->Get_total_length();
    if(length<=0) return;
    scene->clear();
    double scaleX=600./length;
    double scaleY=180./sqrt(System_pointer->Max_Width());
    int x=20;
    //if(ui->checkBoxLeft->isChecked())

    for(unsigned int i=0;i<System_pointer->Get_count();++i)
    {
        Bar_model tmp=System_pointer->Get_bar(i);
        if(i==0&&System_pointer->leftFix) DrawEdge(false,x,50);
        int s=sqrt(tmp.A)*scaleY;
        int l=tmp.L*scaleX;
        scene->addRect(x,155-s/2,l,s);


        if(tmp.LinLoad!=0)
        {
            if (tmp.LinLoad>0) DrawLinLoad(x,x+l);
            if (tmp.LinLoad<0) DrawLinLoad(x+l,x);
        }
        if(tmp.FocLoadLeft!=0)
        {
            if (tmp.FocLoadLeft>0) DrawArrow(Qt::red,x,x+l/2);
            if (tmp.FocLoadLeft<0) DrawArrow(Qt::red,x,x-l/2);
        }
        if(tmp.FocLoadRight!=0)
        {
            if (tmp.FocLoadRight>0) DrawArrow(Qt::red,x+l,x+1.5*l);
            if (tmp.FocLoadRight<0) DrawArrow(Qt::red,x+l,x-0.5*l);
        }
        x+=l;
        if(i==System_pointer->Get_count()-1&& System_pointer->rightFix) DrawEdge(true,x,50);
    }
   //if(ui->checkBoxRight->isChecked())
    ui->graphicsView->setScene(scene);

}


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->lineEditBarLength->setValidator(new QDoubleValidator(this));
    ui->lineEditBarE->setValidator(new QDoubleValidator(this));
    ui->lineEditBarS->setValidator(new QDoubleValidator(this));
    ui->lineEditBarLim->setValidator(new QDoubleValidator(this));
    ui->lineEditLinLoad->setValidator(new QDoubleValidator(this));
    ui->lineEditFocusedLoadLeft->setValidator(new QDoubleValidator(this));
    ui->lineEditFocusedLoadRight->setValidator(new QDoubleValidator(this));

    ui->tableWidgetBar->setColumnCount(7);
    ui->tableWidgetBar->setColumnWidth(0,82);
    ui->tableWidgetBar->setColumnWidth(1,82);
    ui->tableWidgetBar->setColumnWidth(2,82);
    ui->tableWidgetBar->setColumnWidth(3,82);
    ui->tableWidgetBar->setColumnWidth(4,82);
    ui->tableWidgetBar->setColumnWidth(5,82);
    ui->tableWidgetBar->setColumnWidth(6,82);
    ui->tableWidgetBar->setHorizontalHeaderLabels(QString("Length;E;A;[σ];Linear\nload;Focused\nload\nleft;Focused\nload\nright").split(";"));
    scene = new QGraphicsScene;
}

Widget::~Widget()
{
    delete ui;
    delete scene;
}

void Widget::on_pushButtonAdd_clicked()
{
    int row=ui->tableWidgetBar->rowCount();

    ui->tableWidgetBar->setRowCount(row+1);
    double L=ui->lineEditBarLength->text().toDouble();
     double E=ui->lineEditBarE->text().toDouble();
     double A=ui->lineEditBarS->text().toDouble();
     double Lim=ui->lineEditBarLim->text().toDouble();
     double LL=ui->lineEditLinLoad->text().toDouble();
     double FL=ui->lineEditFocusedLoadLeft->text().toDouble();
     double FR=ui->lineEditFocusedLoadRight->text().toDouble();
     Bar_model tmp(L,E,A,Lim,LL,FL,FR);
    ui->tableWidgetBar->setItem(row,0,new QTableWidgetItem(ui->lineEditBarLength->text()));
    ui->tableWidgetBar->setItem(row,1,new QTableWidgetItem(ui->lineEditBarE->text()));
    ui->tableWidgetBar->setItem(row,2,new QTableWidgetItem(ui->lineEditBarS->text()));
    ui->tableWidgetBar->setItem(row,3,new QTableWidgetItem(ui->lineEditBarLim->text()));
    ui->tableWidgetBar->setItem(row,4,new QTableWidgetItem(ui->lineEditLinLoad->text()));
    ui->tableWidgetBar->setItem(row,5,new QTableWidgetItem(ui->lineEditFocusedLoadLeft->text()));
    ui->tableWidgetBar->setItem(row,6,new QTableWidgetItem(ui->lineEditFocusedLoadRight->text()));
    ui->lineEditBarLength->clear();
    ui->lineEditBarE->clear();
    ui->lineEditBarS->clear();
    ui->lineEditBarLim->clear();
    ui->lineEditLinLoad->clear();
    ui->lineEditFocusedLoadLeft->clear();
    ui->lineEditFocusedLoadRight->clear();
    System_pointer->AddBar(tmp);
    Draw();
}


void Widget::on_pushButtonDelete_clicked()
{
    if (ui->tableWidgetBar->selectedItems().size()<1) return;
    int row=ui->tableWidgetBar->row(*(ui->tableWidgetBar->selectedItems().begin()));
    System_pointer->Delete_bar(row);
    ui->tableWidgetBar->removeRow(row);
    Draw();
}



void Widget::on_pushButtonUp_clicked()
{
    if(ui->tableWidgetBar->selectedItems().size()<1) return;
    int row=ui->tableWidgetBar->row(*(ui->tableWidgetBar->selectedItems().begin()));
    if(row<1) return;
    ui->tableWidgetBar->insertRow(row-1);
    ui->tableWidgetBar->setItem(row-1,0,ui->tableWidgetBar->takeItem(row+1,0));
    ui->tableWidgetBar->setItem(row-1,1,ui->tableWidgetBar->takeItem(row+1,1));
    ui->tableWidgetBar->setItem(row-1,2,ui->tableWidgetBar->takeItem(row+1,2));
    ui->tableWidgetBar->setItem(row-1,3,ui->tableWidgetBar->takeItem(row+1,3));
    ui->tableWidgetBar->setItem(row-1,4,ui->tableWidgetBar->takeItem(row+1,4));
    ui->tableWidgetBar->setItem(row-1,5,ui->tableWidgetBar->takeItem(row+1,5));
    ui->tableWidgetBar->setItem(row-1,6,ui->tableWidgetBar->takeItem(row+1,6));
    ui->tableWidgetBar->removeRow(row+1);
    System_pointer->Swap_bars(row,row-1);
    Draw();
   }

void Widget::on_pushButtonDown_clicked()
{
    if(ui->tableWidgetBar->selectedItems().size()<1) return;
    int row=ui->tableWidgetBar->row(*(ui->tableWidgetBar->selectedItems().begin()));
    if(row>ui->tableWidgetBar->rowCount()-2) return;
    ui->tableWidgetBar->insertRow(row+2);
    ui->tableWidgetBar->setItem(row+2,0,ui->tableWidgetBar->takeItem(row,0));
    ui->tableWidgetBar->setItem(row+2,1,ui->tableWidgetBar->takeItem(row,1));
    ui->tableWidgetBar->setItem(row+2,2,ui->tableWidgetBar->takeItem(row,2));
    ui->tableWidgetBar->setItem(row+2,3,ui->tableWidgetBar->takeItem(row,3));
    ui->tableWidgetBar->setItem(row+2,4,ui->tableWidgetBar->takeItem(row,4));
    ui->tableWidgetBar->setItem(row+2,5,ui->tableWidgetBar->takeItem(row,5));
    ui->tableWidgetBar->setItem(row+2,6,ui->tableWidgetBar->takeItem(row,6));
    ui->tableWidgetBar->removeRow(row);
    System_pointer->Swap_bars(row,row+1);
    Draw();
}




void Widget::on_checkBoxLeft_clicked()
{
    System_pointer->setEnds(ui->checkBoxLeft->isChecked(),ui->checkBoxRight->isChecked());
    Draw();
}

void Widget::on_checkBoxRight_clicked()
{
    System_pointer->setEnds(ui->checkBoxLeft->isChecked(),ui->checkBoxRight->isChecked());
    Draw();
}

void Widget::on_pushButtonCompute_clicked()
{
    if(System_pointer->Get_count()<1) return;
    System_pointer->Form_tables();
    try
    {
    processor_pointer->Jordan_Gauss();
    }
    catch (processor_expt expt){}
    TabWidget * test =new TabWidget(processor_pointer,System_pointer);
    test->print_results();
    test->show();
}


void Widget::on_pushButtonSave_clicked()
{
  QString str = QFileDialog::getSaveFileName(0, "Save...", "","*.bar");
  System_pointer->Save(str.toStdString());
}


void Widget::on_pushButtonLoad_clicked()
{
    QString str = QFileDialog::getOpenFileName(0, "Load...", "", "*.bar");
    System_pointer->Load(str.toStdString());

    ui->tableWidgetBar->setRowCount(System_pointer->Get_count());
    for(int i=0;i<System_pointer->Get_count();++i)
    {
        Bar_model bar=System_pointer->Get_bar(i);
    ui->tableWidgetBar->setItem(i,0,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.L))));
    ui->tableWidgetBar->setItem(i,1,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.E))));
    ui->tableWidgetBar->setItem(i,2,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.A))));
    ui->tableWidgetBar->setItem(i,3,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.Lim))));
    ui->tableWidgetBar->setItem(i,4,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.LinLoad))));
    ui->tableWidgetBar->setItem(i,5,new QTableWidgetItem(QString::fromStdString(std::to_string(bar.FocLoadLeft))));
    ui->tableWidgetBar->setItem(i,6,new QTableWidgetItem(QString::fromStdString(std::to_string((bar.FocLoadRight)))));

    }
    ui->checkBoxLeft->setChecked(System_pointer->leftFix);
     ui->checkBoxRight->setChecked(System_pointer->rightFix);
    Draw();

}
