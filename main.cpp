
#include <QApplication>
#include "system_model.h"
#include "processor_model.h"
#include "widget.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    System_model system;
    processor_model processor(&system);

    w.show();
    w.System_pointer=&system;
    w.processor_pointer=&processor;
    system.processor_pointer=&processor;
    return a.exec();
}
