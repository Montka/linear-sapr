
#include<vector>
#include<math.h>
class processor_model;
class System_model;

#include "system_model.h"
#include "processor_model.h"
   void processor_model::Copy_table(double** A, double* B, int c)
    {
        count=c;
        tableB=new double[c];
        tableA=new double*[c];
        for(int i=0;i<c;++i)
            tableA[i]=new double[c];

        for(int i=0;i<c;++i)
        {
            tableB[i]=B[i];
            for(int j=0;j<c;++j)
            tableA[i][j]=A[i][j];
        }
      }

    processor_model::processor_model(System_model * pointer):System_pointer(pointer)
    {
        count=0;
        tableA=NULL;
        tableB=NULL;
    }
    processor_model::~processor_model()
    {
        delete [] tableB;
        for(int i=0;i<count;++i)
            delete [] tableA[i];
        delete [] tableA;
    }
    void processor_model::Jordan_Gauss()
    {
        for(int j=0;j<count;++j)
        {
            double a=tableA[j][j];
            for(int n=0;n<count;++n)
            {
                tableA[j][n]/=a;
            }
            tableB[j]/=a;

            for(int i=0;i<count;++i)
            {
            if(i==j)continue;
            double relation=-tableA[i][j];
            double resolv=0;
            for(int n=0;n<count;++n)
                {
                    tableA[i][n]+=tableA[j][n]*relation;
                    resolv+=abs(tableA[i][n]);
                }

                    tableB[i]+=tableB[j]*relation;

                   if (resolv==0 && tableB[i]!=0)
                        //std::cout<<resolv;
                        throw processor_expt("Undefined system");
            }
     }

    }
    /*
    void processor_model::find_roots()
    {
        x.push_back(Step(y(0)/b(0),-d(0)/b(0),1));
       for(unsigned int i=0;i<x.size();++i)
         std::cout<<x[i]<<std::endl;
    }

    double processor_model::a(int i){return tableA[i][i-1];}
    double processor_model::b(int i){return tableA[i][i];}
    double processor_model::y(int i){return tableA[i][i+1];}
    double processor_model::d(int i){return tableB[i];}


    double processor_model::Step(double Pi, double Qi, int i)
    {
        double xi;
        ++i;
        if (i<count-1)
        {
        double P=y(i)/(b(i)+a(i)*Pi);
        double Q=(a(i)*Qi-d(i))/(b(i)-a(i)*Pi);
        xi=P*Step(P,Q,i)+Q;
        }
        else
        {
          xi=(a(i)*Qi-d(i))/(b(i)-a(i)*Pi);
        }
        x.push_back(xi);
        return xi;
    }
*/

     double processor_model::delta(int p)
     {
         if(p<0 || p>count) return 0;
         return tableB[p];
     }

    double processor_model::u(int p, double x)
    {
        Bar_model bar=System_pointer->Get_bar(p);
        if(p<0 || p>count || x<0 || x>bar.L) return 0;
        return tableB[p]+(x/bar.L)*(tableB[p+1]-tableB[p])+(bar.LinLoad*bar.L*x)/(2*bar.E*bar.A)*(1-x/bar.L);
    }
    double processor_model::N(int p, double x)
    {
        Bar_model bar=System_pointer->Get_bar(p);
        if(p<0 || p>count || x<0 || x>bar.L) return 0;
        return (bar.E*bar.A/bar.L)*(tableB[p+1]-tableB[p])+bar.LinLoad*bar.L/2*(1-2*x/bar.L);
    }
    double processor_model::sigma(int p, double x)
    {
        Bar_model bar=System_pointer->Get_bar(p);
        if(p<0 || p>count || x<0 || x>bar.L) return 0;
        return N(p,x)/bar.A;
    }
