#-------------------------------------------------
#
# Project created by QtCreator 2015-01-06T07:04:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chekanin_Uj
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    system_model.cpp \
    bar_model.cpp \
    processor_model.cpp \
    tabwidget.cpp

HEADERS  += widget.h \
    system_model.h \
    bar_model.h \
    processor_model.h \
    tabwidget.h

FORMS    += widget.ui \
    tabwidget.ui

QMAKE_CXXFLAGS += -std=c++11
