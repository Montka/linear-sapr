#include <assert.h>
#include "bar_model.h"
Bar_model::Bar_model(){}
Bar_model::Bar_model(long double _L, long double _E, long double _A, long double _Lim, long double _LL, long double _FL,long double _FR)
{
    L=_L;
    E=_E;
    A=_A;
    Lim=_Lim;
    LinLoad=_LL;
    FocLoadLeft=_FL;
    FocLoadRight=_FR;

    assert(L>0 && E >0 && A>0);

}
