#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QGraphicsScene>
#include <QColor>
#include <QTabWidget>
#include "processor_model.h"
#include "system_model.h"
namespace Ui {
class TabWidget;
}

class TabWidget : public QTabWidget
{
    Q_OBJECT

public:
    TabWidget(processor_model *,System_model *,QWidget *parent = 0);
    ~TabWidget();
    void print_results();
    processor_model * processor_pointer;
    System_model * system_pointer;
    bool Save(std::string);
private slots:
    void on_pushButtonSave_clicked();

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::TabWidget *ui;
   QGraphicsScene* sceneN ;
   QGraphicsScene* sceneQ ;
   QGraphicsScene* sceneU ;
   void Draw();
   double max_value(int, int);
};

#endif // TABWIDGET_H
