#include <fstream>
#include <vector>
#include <iostream>
#include "bar_model.h"
#include "system_model.h"
#include "processor_model.h"

System_model::System_model()
{
    leftFix=false;
    rightFix=false;
}

void System_model::AddBar(Bar_model bar)
{
    Bars.push_back(bar);
}
void System_model::AddBar(long double L,long double E,long double A,long double Lim,long double LL,long double FL,long double FR)
{
    Bars.push_back(Bar_model(L,E,A,Lim,LL,FL,FR));
}
void System_model::setEnds(bool L, bool R)
{
    leftFix=L;
    rightFix=R;
}

void System_model::Swap_bars(int i, int j)
{
    Bar_model tmp=Bars[i];
    Bars[i]=Bars[j];
    Bars[j]=tmp;
}

void System_model::Delete_bar(int i)
{
    Bars.erase(Bars.begin()+i);
}

bool System_model::Save(std::string name)
{
    std::ofstream fs;

    fs.open(name.c_str(),std::ofstream::binary | std::ofstream::trunc);
    if(!fs){return false;}
    fs.write((char*)&leftFix,sizeof(bool));
    fs.write((char*)&rightFix,sizeof(bool));
    for(unsigned int i=0;i<Bars.size();++i)
    {

        fs.write((char*)&Bars[i],sizeof(Bar_model));

    }
    return true;
}
bool System_model::Load(std::string name)
{
    std::ifstream fs;

    fs.open(name.c_str(),std::ifstream::binary);
    if(!fs){return false;}
    Bars.clear();
    Bar_model bar;
    fs.seekg (0, fs.end);
    int length = fs.tellg();
    fs.seekg (0, fs.beg);
    fs.read((char*)&leftFix,sizeof(bool));
    fs.read((char*)&rightFix,sizeof(bool));

    do{

       fs.read((char*)&bar,sizeof(Bar_model));
       Bars.push_back(bar);


    }while (length!=fs.tellg());


    return true;
}

double System_model::Get_total_length()
{
    unsigned int total=0;
    for(unsigned int i=0; i<Bars.size();++i)
        total+=Bars[i].L;
   return total;
}
double System_model::Get_length(int i)
{
    return Bars[i].L;
}

Bar_model  System_model::Get_bar(int i)
{
    return Bars[i];
}

unsigned int System_model::Get_count()
{
    return Bars.size();
}

double System_model::Max_Width()
{
    double tmp=0;
    for(unsigned int i=0;i<Bars.size();++i)
        if (Bars[i].A>tmp){tmp=Bars[i].A;}
    return tmp;
}
double System_model::tableA_func(int i)
{
    return Bars[i].E * Bars[i].A/Bars[i].L;
}

double System_model::tableB_func(int i)
{
    double res=0;
    if(i>0)
        res+=(Bars[i-1].LinLoad*Bars[i-1].L/2+Bars[i-1].FocLoadRight);
    if(i<Node_count-1)
        res+=(Bars[i].LinLoad*Bars[i].L/2+Bars[i].FocLoadLeft);

    return res;
}

void System_model::Form_tables()
{
    //Memory assign:
    Node_count=Bars.size()+1;
    tableB=new double[Node_count];

    tableA=new double*[Node_count];
            for(int i=0;i<Node_count;++i)
            tableA[i]=new double[Node_count];

    //Initializing:
    for(int i=0;i<Node_count;++i)
    {
        tableB[i]=0;
        for(int j=0;j<Node_count;++j)
            tableA[i][j]=0;
    }
    //Forming table A
    for(int i=0;i<Node_count-1;++i)
    {
        double tmp=tableA_func(i);
        tableA[i][i]+=tmp;
        tableA[i+1][i+1]=tmp;
        tableA[i+1][i]=tableA[i][i+1]=-tmp;
    }
    if(leftFix)
    {
        for(int i=0;i<Node_count;++i)
            tableA[0][i]=0;
        tableA[0][0]=1;
    }
    if(rightFix)
    {

        for(int i=0;i<Node_count;++i)
            tableA[Node_count-1][i]=0;
        tableA[Node_count-1][Node_count-1]=1;
    }

    //Forming table B

    for(int i=0; i<Node_count;++i)
        tableB[i]=tableB_func(i);
    if(leftFix)
        tableB[0]=0;
     if(rightFix)
         tableB[Node_count-1]=0;

     processor_pointer->Copy_table(tableA,tableB,Node_count);

     //TEST SECTION
       /*
       try
       {
       processor.Jordan_Gauss();
       }
       catch (processor_expt expt){std::cout<<expt.msg;}*/
}

