#ifndef WIDGET_H
#define WIDGET_H
#include "system_model.h"
#include "processor_model.h"
#include <QWidget>
#include <QGraphicsScene>
#include <QColor>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    System_model* System_pointer;
    processor_model * processor_pointer;
private slots:
    void on_pushButtonAdd_clicked();

    void on_pushButtonDelete_clicked();

    void on_pushButtonUp_clicked();

    void on_pushButtonDown_clicked();

    void on_checkBoxLeft_clicked();

    void on_checkBoxRight_clicked();

    void on_pushButtonCompute_clicked();



    void on_pushButtonSave_clicked();

    void on_pushButtonLoad_clicked();

private:
    Ui::Widget *ui;
    void Draw();
    void DrawArrow(QColor,int, int);
    void DrawLinLoad(int, int);
    void DrawEdge(bool, int, int);
    QGraphicsScene* scene;

};

#endif // WIDGET_H
