#ifndef BAR_SYSTEM_H
#define BAR_SYSTEM_H
#include <vector>
#include <string>
class processor_model;
class System_model;
#include "bar_model.h"
#include "processor_model.h"
class System_model
{
std::vector<Bar_model> Bars;
//Fixed ends:

double tableA_func(int);
double tableB_func(int);

public:
processor_model * processor_pointer;
int Node_count;
double** tableA;
double* tableB;
bool leftFix;
bool rightFix;
System_model();
void AddBar(Bar_model);
void AddBar(long double,long double,long double,long double,long double,long double,long double);
void setEnds(bool, bool);
void Swap_bars(int, int);
void Delete_bar(int);
bool Save(std::string);
bool Load(std::string);
double Get_total_length();
double Get_length(int);
Bar_model Get_bar(int i);
unsigned int Get_count();
double Max_Width();
void Form_tables();
};
#endif // BAR_SYSTEM_H
