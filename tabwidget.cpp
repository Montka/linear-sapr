#include "tabwidget.h"
#include "ui_tabwidget.h"
#include <string>
#include <iostream>
#include <math.h>
#include <fstream>
#include <QFileDialog>
#include <QGraphicsItem>


TabWidget::TabWidget(processor_model *p, System_model *s, QWidget *parent) :
    QTabWidget(parent),
    ui(new Ui::TabWidget),
    processor_pointer(p),
    system_pointer(s)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(6);
    ui->tableWidget->setColumnWidth(0,83);
    ui->tableWidget->setColumnWidth(1,83);
    ui->tableWidget->setColumnWidth(2,83);
    ui->tableWidget->setColumnWidth(3,83);
    ui->tableWidget->setColumnWidth(4,83);
    ui->tableWidget->setColumnWidth(5,83);
    ui->tableWidget->setHorizontalHeaderLabels(QString("U0;UL;N0;NL;σ0;σL").split(";"));
    sceneN = new QGraphicsScene;
    sceneQ = new QGraphicsScene;
    sceneU = new QGraphicsScene;
    ui->comboBox->addItem("N(x)",QVariant());
    ui->comboBox->addItem("u(x)",QVariant());
    ui->comboBox->addItem("σ(x)",QVariant());
}

TabWidget::~TabWidget()
{
    delete ui;
}

void TabWidget::print_results()
{

    for(int i=0;i<system_pointer->Get_count();++i)
    {
       ui->tableWidget->setRowCount(i+1);
       ui->tableWidget->setItem(i,0,new QTableWidgetItem(QString::number(processor_pointer->u(i,0))));
       ui->tableWidget->setItem(i,1,new QTableWidgetItem(QString::number(processor_pointer->u(i,system_pointer->Get_length(i)))));
       ui->tableWidget->setItem(i,2,new QTableWidgetItem(QString::number(processor_pointer->N(i,0))));
       ui->tableWidget->setItem(i,3,new QTableWidgetItem(QString::number(processor_pointer->N(i,system_pointer->Get_length(i)))));
       ui->tableWidget->setItem(i,4,new QTableWidgetItem(QString::number(processor_pointer->sigma(i,0))));
       if(abs(processor_pointer->sigma(i,0))>abs(system_pointer->Get_bar(i).Lim))
       {ui->tableWidget->item( i, 4)->setBackground( Qt::red);}
       ui->tableWidget->setItem(i,5,new QTableWidgetItem(QString::number(processor_pointer->sigma(i,system_pointer->Get_length(i)))));
       if(abs(processor_pointer->sigma(i,system_pointer->Get_length(i)))>abs(system_pointer->Get_bar(i).Lim))
       {ui->tableWidget->item( i, 5)->setBackground( Qt::red);}

    }
    Draw();
}

void TabWidget::Draw()
{
    QPainterPath pathN,pathU,pathQ;
    QGraphicsItem* text;
    int x=-220;
    sceneN->addLine(-260,0,260,0);
    double scaleX=400/system_pointer->Get_total_length();
    double y1,y2;
    double scaleY;
    int l;
    scaleY=-50/max_value(2,3);
    for(int i=0; i<system_pointer->Get_count();++i)
    {

        l=system_pointer->Get_length(i);
        y1=processor_pointer->N(i,0);
        y2=processor_pointer->N(i,l);

        text=sceneN->addText(QString::number(y1),QFont("Times", 5));
        text->setPos(x+(scaleX*l/6),y1*scaleY-(y1<0?-10:30));
        text=sceneN->addText(QString::number(y2),QFont("Times", 5));
        text->setPos(x+l*scaleX-(scaleX*l/6),y2*scaleY-(y2<0?-10:30));

        pathN.moveTo(x,0);
        pathN.lineTo(x,y1*scaleY);
        pathN.lineTo(x+l*scaleX,y2*scaleY);
        pathN.lineTo(x+l*scaleX,0);
        pathN.closeSubpath();
        x+=l*scaleX;
    }
    scaleY=-50/max_value(4,5);
    for(int i=0; i<system_pointer->Get_count();++i)
    {
        int max=system_pointer->Get_bar(i).Lim;
        l=system_pointer->Get_length(i);
        y1=processor_pointer->sigma(i,0);
        y2=processor_pointer->sigma(i,l);

        sceneQ->addLine(x,max*scaleY,x+l*scaleX,max*scaleY,QPen(Qt::red, 2, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin))->setZValue(10);
        sceneQ->addLine(x,-max*scaleY,x+l*scaleX,-max*scaleY,QPen(Qt::red, 2, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin))->setZValue(10);


        text=sceneQ->addText(QString::number(y1),QFont("Times", 5));
        text->setPos(x+(scaleX*l/6),y1*scaleY-(y1<0?-10:30));
        text->setZValue(5);
        text=sceneQ->addText(QString::number(y2),QFont("Times", 5));
        text->setPos(x+l*scaleX-(scaleX*l/6),y2*scaleY-(y2<0?-10:30));
        text->setZValue(5);
        pathQ.moveTo(x,0);
        pathQ.lineTo(x,y1*scaleY);
        pathQ.lineTo(x+l*scaleX,y2*scaleY);
        pathQ.lineTo(x+l*scaleX,0);
        pathQ.closeSubpath();

        x+=l*scaleX;
    }


    scaleY=-50/max_value(0,1);
    for(int i=0; i<system_pointer->Get_count();++i)
    {

        l=system_pointer->Get_length(i);
        y1=processor_pointer->u(i,0);
        y2=processor_pointer->u(i,l);

        text=sceneU->addText(QString::number(y1),QFont("Times", 4));
        text->setPos(x+(scaleX*l/5),y1*scaleY-(y1<0?-10:30));
        text=sceneU->addText(QString::number(y2),QFont("Times", 4));
        text->setPos(x+l*scaleX-(scaleX*l/5),y2*scaleY-(y2<0?-10:30));

        pathU.moveTo(x,0);
        pathU.lineTo(x,y1*scaleY);
        for(int n=1;n<11;++n)
          pathU.lineTo(x+(n*l/10)*scaleX,processor_pointer->u(i,n*l/10)*scaleY);


        pathU.lineTo(x+l*scaleX,0);
        pathU.closeSubpath();
        x+=l*scaleX;
    }



    sceneU->addPath(pathU,QPen(Qt::SolidLine),QBrush(Qt::gray))->setZValue(1);
    sceneN->addPath(pathN,QPen(Qt::SolidLine),QBrush(Qt::gray))->setZValue(1);
    sceneQ->addPath(pathQ,QPen(Qt::SolidLine),QBrush(Qt::gray))->setZValue(1);

    //ui->graphicsView->setScene(sceneU);
}

double TabWidget::max_value(int x, int y)
{
    double max1=0;
    double max2=0;
    for(int i=0;i<ui->tableWidget->rowCount();++i)
    {
        if (ui->tableWidget->item(i,x)->text().toDouble()>max1)
            max1=ui->tableWidget->item(i,x)->text().toDouble();
        if (ui->tableWidget->item(i,y)->text().toDouble()>max2)
            max2=ui->tableWidget->item(i,y)->text().toDouble();
    }
    return max1>max2?max1:max2;
}

bool TabWidget::Save(std::string name)
{
    std::ofstream fs;
    fs.open(name.c_str());
    if(!fs){return false;}
    for(int i=0;i<ui->tableWidget->rowCount();++i)
    {

        for(int j=0;j<ui->tableWidget->columnCount();++j)
        {
            fs<<ui->tableWidget->item(i,j)->text().toStdString();
            if(ui->tableWidget->item(i,j)->backgroundColor()==Qt::red)fs<<"*";
            fs<<"  ";
        }
            fs<<std::endl;
    }

    return true;
}

void TabWidget::on_pushButtonSave_clicked()
{
    QString str = QFileDialog::getSaveFileName(0, "Open Dialog", "", "*.txt");
    Save(str.toStdString());
}

void TabWidget::on_comboBox_currentIndexChanged(int index)
{
    switch (index) {
    case 0:
        ui->graphicsView->setScene(sceneN);
        break;
    case 1:
        ui->graphicsView->setScene(sceneU);
        break;

    case 2:
        ui->graphicsView->setScene(sceneQ);
        break;
    default:
        break;
    }
}
