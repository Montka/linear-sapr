#ifndef BAR_MODEL_H
#define BAR_MODEL_H
struct Bar_model
{
    long double L;
    long double E;
    long double A;
    long double Lim;
    long double LinLoad;
    long double FocLoadLeft;
    long double FocLoadRight;//Focused load alligned to the LEFT end of bar
    //sry for including FocLoad - its much easier
    Bar_model();
    Bar_model(long double,long double,long double, long double,long double,long double,long double);
};
#endif // BAR_MODEL_H
