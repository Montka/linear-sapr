#ifndef DIALOG_PATH_H
#define DIALOG_PATH_H

#include <QDialog>

namespace Ui {
class Dialog_path;
}

class Dialog_path : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_path(QWidget *parent = 0);
    ~Dialog_path();

private:
    Ui::Dialog_path *ui;
};

#endif // DIALOG_PATH_H
